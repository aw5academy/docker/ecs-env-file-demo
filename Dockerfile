FROM amazonlinux:latest

ADD start.sh /root/start.sh
RUN yum install -y httpd
ADD index.html /var/www/html/index.html
RUN chmod 644 /var/www/html/index.html
RUN chmod +x /root/start.sh
EXPOSE 80
CMD ["/root/start.sh"]
