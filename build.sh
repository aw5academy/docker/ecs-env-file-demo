export AWS_ACCOUNT_ID=`aws sts get-caller-identity |jq -r .Account`
export REGISTRY_HOST="${AWS_ACCOUNT_ID}.dkr.ecr.us-east-1.amazonaws.com"
docker build -t $REGISTRY_HOST/ecs-env-file-demo:latest .
rm -f ~/.docker/config.json
$(aws ecr get-login --no-include-email --region us-east-1)
docker push ${REGISTRY_HOST}/ecs-env-file-demo:latest
