#!/bin/bash

cat <<EOF > /var/www/html/styles.css
body {
  background: ${CSS_BACKGROUND}
}
EOF

/usr/sbin/httpd -D FOREGROUND
